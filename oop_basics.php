<?php
class StudentInfo{
    public $std_id;
    public $std_name;
    public $std_cgpa;

    public function set_std_id($std_id){
        $this->std_id=$std_id;
    }
    public function set_std_name($std_name){
        $this->std_name=$std_name;
    }
    public function set_std_cgpa($std_cgpa){
        $this->std_cgpa=$std_cgpa;
    }

    public function get_std_id(){
        return $this->std_id;
    }
    public function get_std_name(){
        return $this->std_name;
    }
    public function get_std_cgpa(){
        return $this->std_cgpa;
    }
}
$id=new StudentInfo();
$name=new StudentInfo();
$cgpa=new StudentInfo();
$id->set_std_id("SEIP136509");
$name->set_std_name("Umme Batul");
$cgpa->set_std_cgpa(5);

echo "Student ID : ".$id->get_std_id()."<br>";
echo "Student Name : ".$name->get_std_name()."<br>";
echo "Student's CGPA : ".$cgpa->get_std_cgpa();
?>


